<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Role;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role_student = Role::where('name', 'user')->first();
        $role_admin  = Role::where('name', 'admin')->first();

        $employee = new User();
        $employee->name = 'Admin';
        $employee->email = 'admin@admin.com';
        $employee->password = bcrypt('secret');
        $employee->status = '1';
        $employee->save();
        $employee->roles()->attach($role_admin);
    }
}
