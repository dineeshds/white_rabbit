<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\DataTables\Facades\DataTables;
use App\User;
use App\File;
use App\Traits\FilesHelper;

class FileController extends Controller
{
    use FilesHelper;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.file');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $search = $request->input('search')['value'];
        $query = File::getDirectory($search);

        return Datatables::of($query)
            ->addColumn('image', function ($query) {
                $url= asset('storage/'.$query->url);
                return '<img src="'.$url.'" border="0" width="200" class="img-rounded" align="center" />';
            })
            ->addColumn('action', function ( $query) {
               return   ('<button type="button" class="btn btn-xs btn-danger" data-toggle="delete" data-url="'
                       . route('file.destroy', $query->id)
                       . '"><i class="fa fa-trash"></i> Delete</button>&nbsp;');
            })
            ->rawColumns(['action', 'image'])
            ->make(true);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'file' => 'required|file|max:2048|mimes:txt,doc,docx,pdf,png,jpeg,jpg,gif',
        ]);

        $data = $request->all();
        $fileName = $request->file('file')->getClientOriginalName();
        $type = $request->file('file')->getMimeType();
        $file = $this->uploadFile($data['file'], 'white_rabbit');
        File::create([
            'file_name' => $fileName ?? null,
            'type'      => $type ?? null,
            'url'       => $file['url'] ?? null,
            'full_url'  => $file['full_url'] ?? null,
            'created_by'=> auth()->user()->id
        ]);
        return redirect()->route('file.index')->with('alert', ['type' => 'success', 'message' => trans('messages.created', ['item' => 'FIle uploaded'])]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(File $file)
    {
        $file->deleted_by = auth()->user()->id;
        $file->update();

        $file->delete();
        return response()->json(['type' => 'success', 'message' => trans('messages.deleted', ['item' => 'File'])]);
    }
}
