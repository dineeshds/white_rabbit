<?php

namespace App\Traits;

use Illuminate\Support\Str;

Trait FilesHelper
{
    /**
     * @param $file
     * @param $location
     * @return string|null
     */
    protected function uploadFile($file, $location) : ?array
    {
        if (!is_file($file)) {
            return null;
        }
        $uploadedFileOriginalExtension = $file->getClientOriginalExtension();
        $uploadedFileUniqueName = $this->uniqueFileName($uploadedFileOriginalExtension);
        $file->storeAs('public/uploads/'.$location, $uploadedFileUniqueName);
        return [
            'full_url' => url()->to('/public/storage/uploads/'.$location.'/'.$uploadedFileUniqueName),
            'url' => 'uploads/'.$location.'/'.$uploadedFileUniqueName,
            ];
    }

    /**
     * @param $uploadedFileOriginalExtension
     * @return string
     */
    private function uniqueFileName($uploadedFileOriginalExtension) : string
    {
        return time() . '_' . Str::random(5) . '.' . $uploadedFileOriginalExtension;
    }
}
