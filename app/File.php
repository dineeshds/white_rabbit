<?php

namespace App;

use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\File;

class File extends Model
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];

    protected $fillable = ['file_name', 'type', 'url', 'full_url', 'deleted_by', 'created_by'];

    public static function getDirectory($search='')
    {
        $files = File::query()->with(['uploaded']);
        if($search)
			$files = $files->where('file_name', 'regexp', ltrim($search,'*'))->get();
		else
			$files = $files->get();
        return $files;
    }

    public static function getDeletedHistory()
    {
        $files = File::onlyTrashed()->with(['uploaded', 'deleted_by']);

        return $files->get();
    }

    public function uploaded()
    {
        return $this->belongsTo(User::class, 'created_by', 'id');
    }

    public function deleted_by()
    {
        return $this->belongsTo(User::class, 'deleted_by', 'id');
    }
}
