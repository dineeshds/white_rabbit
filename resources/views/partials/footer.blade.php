<footer class="main-footer">
    <div class="pull-right hidden-xs">
        <b>Powered By</b> <a href="https://whiterabbit.group/">WhiteRabbit</a>
    </div>
    <strong>Copyright &copy; 2019 booking.</strong> All rights reserved.
</footer>