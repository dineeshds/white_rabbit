<!-- Left side column. contains the sidebar -->
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                 <img src="{{ get_profile_image(Auth::user()->image) }}" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
                <p>{{ Auth::user()->name }}</p>
                <a href="#">{{ Auth::user()->email }}</a>
            </div>
        </div>
        <!-- /.search form -->
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu" data-widget="tree">
            <li class="header">MAIN NAVIGATION</li>
            @if(Auth::user()->hasRole('admin'))
                <li class="{{ set_active('file') }}"><a href="{{ url('/file') }}"><i class="fa fa-user"></i> <span>Files</span></a></li>
                <li class="{{ set_active('upload-history') }}"><a href="{{ url('/upload-history') }}"><i class="fa fa-user"></i> <span>Upload History</span></a></li>
                <li class="{{ set_active('delete-history') }}"><a href="{{ url('/delete-history') }}"><i class="fa fa-user"></i> <span>Delete History</span></a></li>
            @endif
           
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>

<!-- =============================================== -->