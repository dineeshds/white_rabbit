@extends ('layouts.app')

@section ('header')
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Files
        <small>All Files </small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ route('dashboard') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li class="active">Files</li>
    </ol>
</section>
@endsection

@section('content')
<!-- Main content -->
<section class="content">
    <div class="box box-success">
        <div class="box-header with-border">
            <h3 class="box-title">UPLOAD Files</h3>
            <div class="row">
                <form role="form" method="POST" enctype="multipart/form-data">
                    <div class="col-xs-12">

                        {{ csrf_field() }}
                            <div class="form-group{{ $errors->has('file') ? ' has-error' : '' }}">
                                <label for="file">Upload Formats -  txt,doc,docx,pdf,png,jpeg,jpg,gif</label>
                                <input id="file" type="file" class="form-control" name="file" value="">
                                @if ($errors->has('file'))
                                    <span class="help-block">{{ $errors->first('file') }}</span>
                                @endif
                            </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <button type="submit" class="btn btn-primary" style="float:right">Upload</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <div class="box-body">
        </div>
    </div>
    <!-- Default box -->
    <div class="box box-success">
        <div class="box-header with-border">
            <h3 class="box-title">List of files</h3>
            
        </div>
        <div class="box-body">
            <table class="table table-bordered table-striped" id="table">
                <thead>
                    <tr>
                        <th>File Name</th>
                        <th>Type</th>
                        <th>Image</th>
                        <th>Action</th>
                        
                    </tr>
                </thead>
                <tfoot>
                    <tr>
                        <th>File Name</th>
                        <th>Type</th>
                        <th>Image</th>
                        <th>Action</th>
                      
                    </tr>
                </tfoot>
            </table>
        </div>
        <!-- /.box-body -->
        <div class="overlay" style="display: none;"><i class="fa fa-refresh fa-spin"></i></div>
    </div>
    <!-- /.box -->

</section>
<!-- /.content -->
@endsection

@push('css')

<!-- DataTables -->
<link rel="stylesheet" href="{{ asset('bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css') }}">
  
@endpush

@push('js')

<!-- DataTables -->
<script src="{{ asset('bower_components/datatables.net/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
<!-- Handlebars -->
<script src="{{ asset('plugins/handlebars/handlebars.min.js') }}"></script>
<!-- Bootstrap Confirmation -->
<script src="{{ asset('plugins/bootstrap-confirmation/bootstrap-confirmation.min.js') }}"></script> 
@endpush

@push('script')

<!-- page script -->
<script>
    $(function () {
        var table = $('#table').DataTable({
            processing: true,
            serverSide: true,
            ajax: '{!! route('file.create') !!}',
            columns: [
                { data: 'file_name', name: 'file_name' },
                { data: 'type', name: 'type' },
                { data: 'image', name: 'image' },
                { data: 'action', name: 'action', orderable: false, searchable: false}
            ],
            search: {
                "regex": true
            },
            "fnDrawCallback": function (oSettings) {
                $('[data-toggle=delete]').confirmation({
                    rootSelector: '[data-toggle=confirmation]',
                    onConfirm: function() {
                        $.ajax({
                            url: $(this).data('url'), 
                            data: {
                                "_token" : "{{ csrf_token() }}"
                            },
                            type: 'DELETE',
                            success: function(result) {
                                if(result) {
                                    $('#table').DataTable().ajax.reload();
                                }
                                Alert.show(result.message,result.type);
                            },
                            beforeSend: function(){
                                $(".overlay").show();
                            },
                            complete: function(){
                                $(".overlay").hide();
                            }
                        });
                    }
                });
            }
        });
    });
</script>
  
@endpush