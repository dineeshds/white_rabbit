@extends ('layouts.app')

@section ('header')
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Delete History
        <small>All Deleted Files </small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ route('dashboard') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li class="active">Deleted Files</li>
    </ol>
</section>
@endsection

@section('content')
<!-- Main content -->
<section class="content">

    <!-- Default box -->
    <div class="box box-success">
        <div class="box-header with-border">
            <h3 class="box-title">List of files Deleted</h3>
            
        </div>
        <div class="box-body">
            <table class="table table-bordered table-striped" id="table">
                <thead>
                <tr>
                    <th>File Name</th>
                    <th>Type</th>
                    <th>Upload Time</th>
                    <th>Uploaded By</th>
                    <th>Deleted By</th>
                </tr>
                </thead>
                <tfoot>
                <tr>
                    <th>File Name</th>
                    <th>Type</th>
                    <th>Upload Time</th>
                    <th>Uploaded By</th>
                    <th>Deleted By</th>
                </tr>
                </tfoot>
            </table>
        </div>
        <!-- /.box-body -->
        <div class="overlay" style="display: none;"><i class="fa fa-refresh fa-spin"></i></div>
    </div>
    <!-- /.box -->

</section>
<!-- /.content -->
@endsection

@push('css')

<!-- DataTables -->
<link rel="stylesheet" href="{{ asset('bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css') }}">
  
@endpush

@push('js')

<!-- DataTables -->
<script src="{{ asset('bower_components/datatables.net/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
<!-- Handlebars -->
<script src="{{ asset('plugins/handlebars/handlebars.min.js') }}"></script>
<!-- Bootstrap Confirmation -->
<script src="{{ asset('plugins/bootstrap-confirmation/bootstrap-confirmation.min.js') }}"></script> 
@endpush

@push('script')

<!-- page script -->
<script>
    $(function () {
        var table = $('#table').DataTable({
            processing: true,
            serverSide: true,
            ajax: '{!! route('delete-history.create') !!}',
            columns: [
                { data: 'file_name', name: 'file_name' },
                { data: 'type', name: 'type' },
                { data: 'created_at', name: 'created_at' },
                { data: 'uploaded.name', name: 'uploaded.name' },
                { data: 'deleted_by.name', data: 'deleted_by.name' },
                
                
            ],

        });
    });
</script>
  
@endpush