@extends('layouts.app')
@section('content')
<!-- Main content -->
<section class="content">
<div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">User Home</div>
            </div>
        </div>
    </div>
    <!-- Default box -->
    <div class="box box-success">
        <div class="box-header with-border">
            
            
        </div>
        <div class="box-body">
            <div class="row">
                <div class="col-xs-12">
                    <div class="col-md-6">
                        
                </div>
            </div>
        </div>
           
        <div class="overlay" style="display: none;"><i class="fa fa-refresh fa-spin"></i></div>
    </div>
    <!-- /.box -->

</section>
<!-- /.content -->
@endsection

@push('css')
<link rel="stylesheet" href="{{ asset('dist/css/jquery-ui.css') }}">
<style>
    * {
    margin: 0;
    padding: 0;
}

.seat {
    float: left;
    display: block;
    margin: 5px;
    background: #4CAF50;
    width: 50px;
    height: 50px;
}

.seat-select {
    display: none;
}
/*.seat-select:checked+.seat {
    background: #F44336;
}*/

</style>
 
@endpush

@push('js')

 <script src="{{ asset('dist/js/jquery-ui.js') }}"></script>

@endpush

@push('script')

<!-- page script -->


<script>
    $(document).ready(function(){
      
  $('.datepicker').datepicker({
        format: 'yyyy-dd-mm',
        endDate: '+0d',
        minDate: 0,
        autoclose: true
    });

    });
</script>
  
@endpush
