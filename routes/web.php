<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', function () {
    if(Auth::check()){
    	return redirect('/dashboard');
    }else{
    	return view('auth.login');
    }
});

Route::group(['namespace'=>'Admin','middleware' => ['isAdmin','auth']], function () {
    Route::resource('file', 'FileController');
    Route::resource('upload-history', 'UploadHistoryController');
    Route::resource('delete-history', 'DeleteHistoryController');
});

Auth::routes();

Route::get('/dashboard', 'HomeController@index')->name('dashboard');
